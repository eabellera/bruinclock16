﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(BruinClock16.Startup))]
namespace BruinClock16
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
