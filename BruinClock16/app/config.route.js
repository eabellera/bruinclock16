﻿(function () {
    'use strict';

    var app = angular.module('app');

    // Collect the routes
    app.constant('routes', getRoutes());
    
    // Configure the routes and route resolvers
    app.config(['$routeProvider', 'routes', routeConfigurator]);
    function routeConfigurator($routeProvider, routes) {

        routes.forEach(function (r) {
            $routeProvider.when(r.url, r.config);
        });
        $routeProvider.otherwise({ redirectTo: '/' });
    }

    // Define the routes 
    function getRoutes() {
        return [
            {
                url: '/',
                config: {
                    templateUrl: 'app/dashboard/dashboard.html',
                    title: 'dashboard',
                    settings: {
                        nav: 1,
                        content: '<i class="fa fa-dashboard"></i> Dashboard'
                    }
                }
            }, {
                url: '/admin',
                config: {
                    title: 'admin',
                    templateUrl: 'app/admin/admin.html',
                    settings: {
                        nav: 2,
                        content: '<i class="fa fa-lock"></i> Admin'
                    }
                }
            }, {
                url: '/employee-hours',
                config: {
                    title: 'employee-hours',
                    templateUrl: 'app/employee-hours/employeeHours.html',
                    settings: {
                        nav: 3,
                        content: 'Employee Hours'
                    }
                }
            }, {
                url: '/pay-periods',
                config: {
                    title: 'pay-periods',
                    templateUrl: 'app/admin/admin.html',
                    settings: {
                        nav: 4,
                        content: 'Pay Periods'
                    }
                }
            }, {
                url: '/employee-manager',
                config: {
                    title: 'employee-manager',
                    templateUrl: 'app/admin/admin.html',
                    settings: {
                        nav: 5,
                        content: 'Employee Manager'
                    }
                }
            }, {
                url: '/hourly-codes',
                config: {
                    title: 'hourly-codes',
                    templateUrl: 'app/admin/admin.html',
                    settings: {
                        nav: 6,
                        content: 'Hourly Codes'
                    }
                }
            }, {
                url: '/reports',
                config: {
                    title: 'reports',
                    templateUrl: 'app/reports/payrollList.html',
                    settings: {
                        nav: 7,
                        content: 'Reports'
                    }
                }
            }, {
                url: '/import-export',
                config: {
                    title: 'import-export',
                    templateUrl: 'app/admin/admin.html',
                    settings: {
                        nav: 8,
                        content: 'Import/Export Agent'
                    }
                }
            }
        ];
    }
})();